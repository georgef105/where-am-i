import {useCallback, useEffect, useRef, useState} from "react";

export type LocationOptions = {
  updatePeriod: number;
} & PositionOptions;

export type Location = {};
export const useMyLocation = (options: LocationOptions) => {
  const [location, setLocation] = useState<{
    position: GeolocationCoordinates;
    pointDiff: number | undefined;
    timeDiff: number | undefined;
    readingAge: number | undefined;
  }>();

  const previousPositionRef = useRef<GeolocationPosition>();

  const [error, setError] = useState<GeolocationPositionError>();

  const handleLocationUpdate = useCallback((pos: GeolocationPosition) => {
    const {coords, timestamp} = pos;
    const pointDiff =
      previousPositionRef.current &&
      getDistanceBetween(previousPositionRef.current.coords, coords);

    const timeDiff = previousPositionRef.current?.timestamp
      ? timestamp - previousPositionRef.current.timestamp
      : 0;

    const readingAge = previousPositionRef.current?.timestamp
      ? Date.now() - previousPositionRef.current?.timestamp
      : 0;

    setLocation({
      position: coords,
      pointDiff,
      timeDiff,
      readingAge,
    });

    previousPositionRef.current = pos;
  }, []);

  useEffect(() => {
    const timer = setInterval(
      () =>
        navigator.geolocation.getCurrentPosition(
          (updatedPosition) => handleLocationUpdate(updatedPosition),
          (e) => setError(e),
          {
            enableHighAccuracy: true,
            timeout: 50,
          }
        ),
      options.updatePeriod
    );

    return () => clearInterval(timer);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return {
    position: location?.position,
    pointDiff: location?.pointDiff,
    timeDiff: location?.timeDiff,
    readingAge: location?.readingAge,
    error,
  };
};

const R = 6371000;

export const getDistanceBetween = (
  point1: GeolocationCoordinates,
  point2: GeolocationCoordinates
): number => {
  const dLat = convertToRad(point2.latitude - point1.latitude);
  const dLon = convertToRad(point2.longitude - point1.longitude);

  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(convertToRad(point1.latitude)) *
      Math.cos(convertToRad(point2.latitude)) *
      Math.sin(dLon / 2) *
      Math.sin(dLon / 2);

  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  return R * c;
};

const convertToRad = (d: number): number => d * (Math.PI / 180);
