import React from "react";
import "./App.css";
import {LocationSummary} from "./LocationSumary/LocationSumary";

function App() {
  return (
    <div className="App">
      <LocationSummary />
    </div>
  );
}

export default App;
