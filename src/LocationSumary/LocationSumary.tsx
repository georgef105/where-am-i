import React, {ComponentType, useState} from "react";
import {LocationOptions, useMyLocation} from "../hooks/useMyLocation";

const DEFAULT_LOCATION_OPTIONS: LocationOptions = {
  enableHighAccuracy: true,
  timeout: 50,
  updatePeriod: 2000,
  maximumAge: 0,
};

type LocationProps = {};

export const LocationSummary: ComponentType<LocationProps> = () => {
  const [bluetoothDevices, setBluetoothDevices] = useState<BluetoothDevice[]>();
  const [locationOptions, setLocationOptions] = useState<LocationOptions>(
    DEFAULT_LOCATION_OPTIONS
  );
  const {position, error, pointDiff, timeDiff, readingAge} = useMyLocation({
    enableHighAccuracy: true,
    timeout: 50,
    updatePeriod: 2000,
  });

  const handleOptionChange = <O extends keyof LocationOptions>(
    option: O,
    value: LocationOptions[O]
  ) => {
    setLocationOptions((previous) => ({
      ...previous,
      [option]: value,
    }));
  };

  const getBluetoothDevices = async () => {
    try {
      const devices = await navigator.bluetooth.getDevices();
      setBluetoothDevices(devices);
    } catch (e) {
      console.log("Error getting devices", e);
      alert("Error getting devices");
    }
  };

  return (
    <>
      <button onClick={getBluetoothDevices}>Get Bluetooth devices</button>
      {bluetoothDevices && (
        <div>
          <p>Bluetooth devices</p>
          {bluetoothDevices.map((device) => (
            <p>
              id: {device.id}, name: {device.name}
            </p>
          ))}
        </div>
      )}
      {position && (
        <div>
          <p>Accuracy {position.accuracy}</p>
          <p>Latitude {position.latitude}</p>
          <p>Longitute {position.longitude}</p>
          <br></br>
          <p>
            AltitudeAccuracy{" "}
            {position.altitudeAccuracy === null
              ? "null"
              : position.altitudeAccuracy}
          </p>
          <p>Altitude {position.altitude}</p>
          <br></br>
          <p>Heading {position.heading}</p>
          <p>Speed {position.speed}</p>
          <br></br>
          <p>Point diff: {pointDiff}</p>
          <p>Time diff: {timeDiff}</p>
          <p>Reading age: {readingAge}</p>
        </div>
      )}
      {error && <p>Error: {JSON.stringify(error)}</p>}
      {!position && <p>Loading</p>}
      <p>Options:</p>
      <div>
        <div style={{marginBottom: "8px"}}>
          <span>enableHighAccuracy: </span>
          <input
            type="checkbox"
            onChange={() =>
              handleOptionChange(
                "enableHighAccuracy",
                !locationOptions.enableHighAccuracy
              )
            }
            checked={locationOptions.enableHighAccuracy}
          />
        </div>
        <div style={{marginBottom: "8px"}}>
          <span>timeout: </span>
          <input
            type="number"
            value={locationOptions.timeout}
            onChange={(event) =>
              handleOptionChange("timeout", +event.target.value)
            }
          />
        </div>
        <div style={{marginBottom: "8px"}}>
          <span>updatePeriod: </span>
          <input
            type="number"
            value={locationOptions.updatePeriod}
            onChange={(event) =>
              handleOptionChange("updatePeriod", +event.target.value)
            }
          />
        </div>
        <div style={{marginBottom: "8px"}}>
          <span>maximumAge: </span>
          <input
            type="number"
            value={locationOptions.maximumAge}
            onChange={(event) =>
              handleOptionChange("maximumAge", +event.target.value)
            }
          />
        </div>
      </div>
    </>
  );
};
